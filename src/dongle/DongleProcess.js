import { app } from 'electron';
import ChildProcess from 'child_process'; //Import for starting the dongle_server app
import path from 'path';
import * as fs from 'fs';

let childProcess;
let intendedExit = false;
const isDevMode = false;

app.on('before-quit', () => {
  intendedExit = true;
});


const killProcess = () => {
  childProcess.kill();
};


const launchDongleServer = () => {
  //start the dongle server on app start:
  //check environment (osx / windows):
  let serverPath;
  if (__production) {
    if (process.platform === 'darwin') {
      console.log('cwd:', process.cwd);
      console.log('process.execPath', process.execPath);

      // serverPath = path.resolve(process.cwd(),  'Contents', 'Resources', 'app', '.webpack', 'main', 'static', 'dongle_server', 'osx','variable_dongle_server'); //works from command line @ .app dir
      serverPath = path.join(process.execPath,  '/../../', 'Resources', 'app', '.webpack', 'main', 'static', 'dongle_server', 'osx','variable_dongle_server');
      try {
        fs.chmodSync(serverPath, '777');
      } catch (error) {
        console.log('chmod error: ', error);
      }
      // serverPath = path.resolve(process.cwd(), 'Contents', 'Resources', 'app', '.webpack', 'main', 'static');//works from command line @ .app dir
      serverPath = path.join(process.execPath,  '/../../', 'Resources', 'app', '.webpack', 'main', 'static');
    }  else {
      serverPath = path.resolve(process.cwd(), 'resources', 'app', '.webpack', 'main', 'static');
    }
    
  } else {
    serverPath = path.resolve(__static);
  }

  console.log('basePath: ', serverPath)
  if (process.platform === 'darwin') {
    serverPath = path.resolve(serverPath, 'dongle_server', 'osx', 'variable_dongle_server');
  } else if (process.platform === 'win32') { // fyi - process.platform returns win32 even on win64
    serverPath = path.resolve(serverPath, 'dongle_server', 'windows', 'variable_dongle_server.exe');
  }


  if (!isDevMode) {
    //Don't use spawn for release (avoid requiring shell / etc)
    childProcess = ChildProcess.execFile(serverPath, [], (error, stdout, stderr) => {
      //Called with the output when process terminates
      if (error) {
        console.log('dongle_server ended with error: ', error);
      } else if (stderr) {
        console.log('dongle_server ended with error: ', error);
      } else if (stdout) {
        console.log('dongle_server ended with stdout: ', stdout);
      }

      if (intendedExit) {
        return;
      }
      //try to restart:
      setTimeout(() => {
        if (intendedExit) {
          return;
        }
        launchDongleServer();
      }, 1000);
    });
    return;
  }
  childProcess = ChildProcess.spawn(serverPath, ['-verbose']);
  childProcess.stdout.on('data', (data) => {
    console.log(`dongle-server:stdout: ${data}`);
  });

  childProcess.stderr.on('data', (data) => {
    console.log(`dongle-server:stderr: ${data}`);
  });

  childProcess.on('close', (code) => {
    console.log(`dongle-server: exited with code ${code}`);
    if (intendedExit) {
      return;
    }
    setTimeout(() => {
      if (intendedExit) {
        return;
      }
      console.log('dongle-server: restarting');
      launchDongleServer();
    }, 1000);
  });
};


export { launchDongleServer, isDevMode, killProcess };
