//
//  Variable Dongle  Demo App
//  App Created using https://electronforge.io
//
//  Copyright Variable, Inc 2019
//
// See https://nodejs.org/dist/latest-v6.x/docs/api/net.html for docs on net
//

import net /* , { Socket } */ from 'net'; //Import for Socket Communication
import { Spectro, ColorScan, Spectrum, Lab } from '../model';

const { dialog } = require('electron').remote;


export interface DongleIOListener {
  //dongle connected / dongle disconnected
  onDongleUpdate: () => void;

  //device connected / disconnected / was licensed / license removed / etc
  //also used for on battery/calibration updated as this affects spectro objects too
  onDevicesUpdate: () => void;

  onColorScan: (serial: string, scan: ColorScan) => void;
  onCalSuccess: (serial: string) => void;
  onLicenseSucecss: () => void;
  onError: (error: Error) => void;
}

// timer - keep retrying until socket connected
const SocketState = {
  DISCONNECTED: 1,
  CONNECTED: 2,
};

class DongleIO {
  firmware: string;
  socketState: number;
  status: string;
  licenseDirectory: string;

  socketInterval: any;
  socketClient: any;
  dongleListener: DongleIOListener;
  readyListener: (error: Error) => void;

  allDevices: Array<Spectro>;
  connectedDevices: Array<Spectro>;
  clearToScan: boolean;

  // Devices array can be used to track connection state & request connect/disconnect
  //  -> ie = View -> req connect -> dongleIO.devices[idx].connect -> Spectro
  devices: Array<Spectro>;
  gotDongle: boolean;
  gotConfig: boolean;


  constructor() {
    this.socketState = SocketState.DISCONNECTED;
    this.status = 'disconnected';
    this.allDevices = [];
    this.connectedDevices = [];

    this.devices = [];
    this.clearToScan = true;

  }

  connectToDongleServer = () => {
    console.log('Starting Socket IO....');
    try {
      this.socketClient = net.connect({ host: 'localhost', port: 9100 }, this._onSocketOpened);
      this.socketClient.on('data', this._onData);
      this.socketClient.on('close', this._onSocketClosed);
    } catch (error) {
      this._onServerDisconnected(error);
      console.log('Socket Connect Failed');
      this.socketState = SocketState.DISCONNECTED;
      if (this.dongleListener) {
        this.dongleListener.onDongleUpdate();
        this.dongleListener.onError(error);
      }
      this._onSocketClosed();
    }
  }

  shutdownServer = () => {
    const cmd = { command: 'Shutdown' };
    this._sendCmd(cmd);
  }


  _onSocketOpened = () => {
    this.socketState = SocketState.CONNECTED;
    if (this.dongleListener) {
      this.dongleListener.onDongleUpdate();
    }
    console.log('Connected to Dongle Server!');
    this.refreshDongleInfo();
  }

  _onSocketClosed = (data: any) => {
    this.socketState = SocketState.DISCONNECTED;
    console.log('onSocketFail: ', data.toString());
    setTimeout(() => {
      if (!this) {
        return;
      }
      if (this.socketState === SocketState.CONNECTED) {
        return;
      }
      this.connectToDongleServer();
    }, 2000);
  }

  connectSpectro = (serial: string) => {
    const idx = this.devices.findIndex(d => d.serial === serial);
    if (idx < 0) {
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('No license for device'));
      }
      return;
    }
    const cmd = { command: 'Connect', parameters: { serial } };
    this._sendCmd(cmd);
  }

  disconnectSpectro = (serial: string) => {
    const idx = this.devices.findIndex(d => d.serial === serial);
    if (idx < 0) {
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('No license for device'));
      }
      return;
    }
    const cmd = { command: 'Disconnect', parameters: { serial } };
    this._sendCmd(cmd);
  }

  scanColor = (serial: string) => {
    if (!this.clearToScan) {
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('5 seconds required between scans'));
      }
      return;
    }
    const idx = this.devices.findIndex(d => d.serial === serial);
    if (idx < 0) {
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('No license for device'));
      }
      return;
    }
    const lab = {
      illuminant: 'D65',
      observer: 'TEN_DEGREE',
    };
    const cmd = { command: 'Scan', parameters: { serial, lab } };
    this._sendCmd(cmd);
  }

  setCalibration = (serial: string, wSense: Array<number>, gSense: Array<number>, bSense: Array<number>) => {
    const sampleScans = { white_sense_values: wSense, green_sense_values: gSense, blue_sense_values: bSense };
    const cmd = { command: 'SETCALIBRATION', parameters: { serial, sample_scans: sampleScans } };
    this._sendCmd(cmd);
  }

  _onCalibrationResult = (respObj) => {
    if (respObj.error_code) {
      console.log('Error onCalibrationResult: ', respObj.error_code);
      if (this.dongleListener) {
        this.dongleListener.onError(new Error(respObj.error_code));
      }
      return;
    }
    const { payload } = respObj;
    const { serial, calibration_result: result } = payload;
    if (result !== 'success') {
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('Calibration Failed: ', result));
      }
      return;
    }
    if (this.dongleListener) {
      this.dongleListener.onCalSuccess(serial);
    }
  }

  _onColorScan = (respObj) => {
    setTimeout(() => {
      if (!this) {
        return;
      }
      this.clearToScan = true;
    }, 5000);
    this.clearToScan = false;
    if (respObj.error_code) {
      console.log('Error onColorScan: ', respObj.error_code);
      if (this.dongleListener) {
        this.dongleListener.onError(new Error(respObj.error_code));
      }
      return;
    }
    const { payload } = respObj;
    const { serial, hex, scan_count: scanCount, sense_values: senseValues } = payload;
    const spectrum = payload.curve !== undefined ? Spectrum.fromJSON(payload) : undefined;
    const lab = Lab.fromJSON(payload.lab);

    const colorScan = Object.assign(new ColorScan(), { spectrum, hex, lab, senseValues });
    if (this.dongleListener) {
      this.dongleListener.onColorScan(serial, colorScan);
    }

    const idx = this.devices.findIndex(d => d.serial === serial);
    if (idx === -1) {
      return;
    }
    this.devices[idx].scanCount = scanCount;
    if (this.dongleListener) {
      this.dongleListener.onDevicesUpdate();
    }
  }

  _onSpectroConnectionUpdate = (response: Object) => {
    const { event, payload, error_code: errorCode } = response;
    if (errorCode) {
      if (this.dongleListener) {
        if (payload && payload.serial) {
          const idx = this.devices.findIndex(d => d.serial === payload.serial);
          if (idx < 0) { return; }
          this.devices[idx].connectionStatus = 'DISCONNECTED';
          this.dongleListener.onDevicesUpdate();
        } else {
          //catch all to ensure we're keeping track of connection to all devs:
          this.refreshDongleInfo();
        }
        this.dongleListener.onError(new Error(errorCode));
      }
      return;
    }
    switch (event) {
      case 'Connect':
        {
          const { serial } = payload;
          const idx = this.devices.findIndex(d => d.serial === serial);
          if (idx < 0) { return; }
          this.devices[idx].connectionStatus = 'CONNECTING';
          if (this.dongleListener) {
            this.dongleListener.onDevicesUpdate();
          }
        }
        break;
      case 'DeviceConnected':
        {
          const { serial } = payload;
          const idx = this.devices.findIndex(d => d.serial === serial);
          if (idx < 0) { return; }
          this.devices[idx] = Spectro.fromJSON(payload);
          this.devices[idx].connectionStatus = 'CONNECTED';
          if (this.dongleListener) {
            this.dongleListener.onDevicesUpdate();
          }
        }
        break;
      case 'DeviceDisconnected':
        {
          const { serial } = payload;
          const idx = this.devices.findIndex(d => d.serial === serial);
          if (idx < 0) { return; }
          this.devices[idx].connectionStatus = 'DISCONNECTED';
          if (this.dongleListener) {
            this.dongleListener.onDevicesUpdate();
          }
        }
        break;
      default:
        break;
    }
  }

  refreshDongleInfo = () => {
    this.gotDongle = false;
    this.gotConfig = false;

    if (this.socketState !== SocketState.CONNECTED) {
      console.log('trying to send when dongle socket closed');
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('USB Dongle Connection Closed'));
      }
      return;
    }
    // console.log('_sendCmd: ', command);
    //  Add \n after each command - JSONL
    this.socketClient.write(`{ "command": "GetDongle"}\n{"command": "GetConfiguration"}\n`);

    // // setTimeout(() => {
    // this._getDongle();
    // // }, 450);
    // // setTimeout(() => {
    // this._getConfiguration();
    // // }, 950);
  }


  _onDongleInfo = () => {
    if (!this.gotDongle || !this.gotConfig) {
      return;
    }

    const { connectedDevices, allDevices } = this;
    const devices = allDevices.map((dev) => {
      const cdi = connectedDevices.findIndex(cd => cd.serial === dev.serial);
      if (cdi !== -1) {
        return connectedDevices[cdi];
      }
      dev.connectionStatus = 'DISCONNECTED';
      return dev;
    });
    this.devices = devices;
    if (this.dongleListener) {
      this.dongleListener.onDevicesUpdate();
    }
  }


  _sendCmd = (command: Object) => {
    if (this.socketState !== SocketState.CONNECTED) {
      console.log('trying to send when dongle socket closed');
      if (this.dongleListener) {
        this.dongleListener.onError(new Error('USB Dongle Connection Closed'));
      }
      return;
    }
    console.log('_sendCmd: ', command);
    //  Add \n after each command - JSONL
    this.socketClient.write(`${JSON.stringify(command)}\n`);
  }

  _getDongle = () => {
    const cmd = { command: 'GetDongle' };
    this._sendCmd(cmd);
  }

  _gotDongle = (payload: Object) => {
    this.gotDongle = true;
    this.status = payload.status;
    this.dongleFw = undefined;
    if (payload.status === 'disconnected') {
      console.log('GetDongle - status:', payload.status);
      this._onDongleInfo();
      return;
    }
    const { connected_devices: connectedDevJSONs, dongle_id: dongleID, status, firwmare_version } = payload;
    const count = connectedDevJSONs ? connectedDevJSONs.length : 0;
    this.dongleFw = firwmare_version;
    console.log('GetDongle - ID:', dongleID, ', status:', status, ', Device Count: ', count, ', DongleFW:', firwmare_version);
    this.connectedDevices = (connectedDevJSONs || []).map((json) => {
      const s = Spectro.fromJSON(json);
      s.connectionStatus = 'CONNECTED';
      return s;
    });
    this._onDongleInfo();
  }

  _gotConfiguration = (respObj: Object) => {
    this.gotConfig = true;
    if (respObj.error_code) {
      console.log('Error-gotConfiguration: ', respObj.error_code);
      if (this.dongleListener) {
        this.dongleListener.onError(new Error(respObj.error_code));
      }
      return;
    }
    const { payload } = respObj;
    const {
      address, license_dir: licenseDir, port, verbose, version, serials, devices } = payload;
    console.log('gotConfiguration: ', address, licenseDir, port, verbose, version, serials);
    // this.serials = [...Array(20)].map((_, i) => `${serials[0]}-${i}`); // <-- for debugging scroller
    this.allDevices = serials.map(serial => {
      const {device_type: deviceType, specular_component: specularComponent} = devices[serial];
      const s = new Spectro();
      s.serial = serial;
      s.deviceType = deviceType;
      s.specularComponent = specularComponent;
      return s;
    });
    this.licenseDirectory = licenseDir;
    this._onDongleInfo();
  }

  _getConfiguration = () => {
    const cmd = { command: 'GetConfiguration' };
    this._sendCmd(cmd);
  }


  _onButtonPress = (respObj) => {
    if (respObj.error_code) {
      console.log('Error - onButtonPress: ', respObj.error_code);
      if (this.dongleListener) {
        this.dongleListener.onError(new Error(respObj.error_code));
      }
      return;
    }
    if (respObj.payload && respObj.payload.serial) {
      const { serial } = respObj.payload;
      this.scanColor(serial);
    } else if (this.dongleListener) {
      this.dongleListener.onError(new Error('Invalid Button Press Event'));
    }
  }


  installLicense = (path: string, overWrite: boolean) => {
    //to test
    const parameters = { file: path, overwrite: overWrite };
    const cmd = { command: 'CopyLicense', parameters };
    this._sendCmd(cmd);
  }

  _licenseResponse = (respObj: Object) => {
    if (respObj.error_code) {
      console.log('Error-licenseResponse: ', respObj.error_code);
      if (this.dongleListener) {
        this.dongleListener.onError(new Error(respObj.error_code));
      }
      return;
    }
    const { payload } = respObj;
    console.log('license updated: ', JSON.stringify(payload));
    if (this.dongleListener) {
      this.dongleListener.onLicenseSucecss();
    }
  }

  removeLicense = (serial: string) => {
    const parameters = { serial };
    const cmd = { command: 'DeleteLicense', parameters };
    this._sendCmd(cmd);
  }

  removeAllLicenses = () => {
    const options = {
      type: 'warning',
      noLink: true,
      title: 'Warning',
      message: 'Delete All Licenses',
      detail: 'Are you sure you want to continue?',
      buttons: ['Cancel', 'Delete All'],
      cancelId: 0,
    };
    dialog.showMessageBox(options, (buttonIndex) => {
      if (buttonIndex > 0) {
        console.log('remove all licenses requested');
        const parameters = { delete_all: true };
        const cmd = { command: 'DeleteLicense', parameters };
        this._sendCmd(cmd);
      }
    });
  }


  // Dongle Server Response Handler:

  _onData = (data: any) => {
    const commandsArray = data.toString().split('\n');
    for (let i = 0; i < commandsArray.length; i++) {
      if (!commandsArray[i]) {
        continue;
      }
      console.log('RX Event: ', commandsArray[i]);
      const respObj = JSON.parse(commandsArray[i]);
      const { event } = respObj;
      if (event.toLowerCase().indexOf('connect') !== -1) {
        this._onSpectroConnectionUpdate(respObj);
        return;
      }
      switch (event) {
        case 'GetDongle':
          this._gotDongle(respObj.payload);
          break;
        case 'GetConfiguration':
          this._gotConfiguration(respObj);
          break;
        case 'CopyLicense':
        case 'DeleteLicense':
          this._licenseResponse(respObj);
          break;
        case 'Scan':
          this._onColorScan(respObj);
          break;
        case 'SETCALIBRATION':
          this._onCalibrationResult(respObj);
          break;
        case 'ButtonDidPress':
          this._onButtonPress(respObj);
          break;
        default:
          break;
      }
    }
  }

}

// Create dongleIO object on app start for Dongle Comms:
const dongleIO = new DongleIO();
export default dongleIO;
export { SocketState };
