//const { app, BrowserWindow } = require('electron');
import { app, BrowserWindow, systemPreferences, shell, Menu, MenuItem, dialog } from 'electron';
import path from 'path';
import {isDevMode, launchDongleServer, killProcess} from './dongle/DongleProcess';

const electron = require('electron')

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let skipDialog = false;

const createWindow = () => {
  // Create the browser window.
  const width = isDevMode ? 1600 : 800;
  const height = process.platform === 'darwin' ? 700 : 750;
  const webPreferences = {nodeIntegration: true, webSecurity: false};
  mainWindow = new BrowserWindow({ show: false, width, height, webPreferences});
  mainWindow.setMinimumSize(width, height);
  mainWindow.setMaximumSize(width, height);

  // and load the index.html of the app.
  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  // Open the DevTools.
  if (isDevMode) {
    mainWindow.webContents.openDevTools();

    electron.globalShortcut.register('CommandOrControl+R', function() {
      console.log('CommandOrControl+R is pressed')
      mainWindow.reload()
    })
  }

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
  mainWindow.on('close', (e) => {
    if (skipDialog){
      return;
    }
    const options = {
      type: 'question',
      noLink: true,
      title: 'Stop Dongle Server',
      message: 'Do you want to stop all bluetooth connections before quitting?',
      buttons: ['Yes', 'No'],
    };
    const choice = dialog.showMessageBoxSync(mainWindow, options);
    if (choice === 0){
      //Use DongleIO Shutdown command to let dongle server shut itself down
      e.preventDefault();
      skipDialog = true;
      mainWindow.webContents.send('request-server-shutdown');
      setTimeout(() => { app.quit(); }, 100);
    } else {
      //Kill the dongle server process
      killProcess();
    }
  });

  const menuItem = new MenuItem({
    label: 'Delete All Serials',
    click: () => {
      mainWindow.webContents.send('delete-all-serials', 'whoooooooh!');
    },
  });

  const isMac = process.platform === 'darwin';
  const template = [
  // { role: 'appMenu' }
    ...(isMac ? [{
      label: 'Variable Dongle Demo',
      submenu: [
        { role: 'about' },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' },
      ],
    }] : []),
    // { role: 'editMenu' }
    {
      label: 'Edit',
      submenu: [
        { type: 'separator' },
        { role: 'copy' },
        menuItem,
      ],
    },
    // { role: 'viewMenu' }
    {
      label: 'View',
      submenu: [
        { role: 'reload' },
        { role: 'forcereload' },
        { role: 'toggledevtools' },
        { type: 'separator' },
      ],
    },
    {
      role: 'help',
      submenu: [
        {
          label: 'Documentation',
          click() { shell.openExternal('https://bitbucket.org/variablecolor/dongle-demo-electron'); },
        },
      ],
    },
  ];
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));

};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  const loading = new BrowserWindow({ show: false, frame: false, width: 400, height: 400, backgroundColor: '#222' });
  loading.webContents.once('dom-ready', async () => {
    launchDongleServer();
    createWindow();
    mainWindow.webContents.once('dom-ready', () => {
      console.log('main loaded');
      mainWindow.show();
      loading.hide();
      loading.close();
    });
  });
  const loadingPath = path.resolve(__static, 'loading.html');
  loading.loadURL(`file://${loadingPath}`);
  loading.show();
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

if (process.platform === 'darwin'){
  systemPreferences.setUserDefault('NSRequiresAquaSystemAppearance', 'boolean', false);
  systemPreferences.setUserDefault('NSDisabledDictationMenuItem', 'boolean', true);
  systemPreferences.setUserDefault('NSDisabledCharacterPaletteMenuItem', 'boolean', true);
}
app.requestSingleInstanceLock();

