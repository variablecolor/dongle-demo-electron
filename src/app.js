//
//  Variable Dongle  Demo App
//  App Created using https://electronforge.io
//
//  Copyright Variable, Inc 2019
//


import React from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import MainView from './components/MainView';

export default class App extends React.Component {
  render() {
    return (<MainView />);
  }
}

ReactDOM.render(<AppContainer><App /></AppContainer>, document.getElementById('App'));
