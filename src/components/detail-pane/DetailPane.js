import React from 'react';
import styled from 'styled-components';
import { Spectro, ColorScan } from '../../model';

import DeviceInfoHeader from './DeviceInfoHeader';
import ColorDetailView from './ColorDetailView';
import { ButtonsContainer, Btn } from '../common';

const MainContent = styled.div`
  position: absolute;
  left: 180px;
  top: 50px;
  bottom: 0;
  right: 0;

  
  box-sizing: border-box;
  padding: 10px;
  border-left-color: #111;
  border-left-width: 2px;
  border-left-style: solid;
`;

const DeviceInfo = styled.div`
  width: 100%;
  height: 60%;
  margin: 10px;
  margin-bottom: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const HeaderText = styled.h2`
  color: #ccc;
  text-align: center;
  margin: 10px;
`;

const Text = styled.div`
  font-size: 15px;
  color: #aaa;
  text-align: center;
  margin: 10px;
`;

const Spacer = styled.div`
  flex: 1;
  width: 100%;
  background-color: #888;
`;

type Props = {
  device: Spectro,
  colorScan: ColorScan,
  onScanClick: (serial: string)=>void,
  onCalClick: (serial: string)=>void,
  onConnectClick: (serial: string)=>void,
  onDisconnectClick: (serial: string)=>void,
};

export default class DetailPane extends React.Component<Props, any>{

  _onScan = () => {
    const {onScanClick, device} = this.props;
    onScanClick(device.serial);
  }
  _onCal = () => {
    const {onCalClick, device} = this.props;
    onCalClick(device.serial);
  }

  _onConn = () => {
    const {onConnectClick, device} = this.props;
    onConnectClick(device.serial);
  }

  _onDisconnect = () => {
    const {onDisconnectClick, device} = this.props;
    onDisconnectClick(device.serial);
  }

  _renderNoneSelected = () => {
    const txt = 'Variable Dongle Demo';
    const instr = 'Click "+" in the left pane to add a device-license or select from aleady installed devices';
    return (
      <MainContent>
        <DeviceInfo>
          <HeaderText>{txt}</HeaderText>
          <Text>{instr}</Text>
        </DeviceInfo>
        <Spacer />
      </MainContent>
    );
  }

  _renderBtns = (connectionState) => {
    if (connectionState !== 'CONNECTED'){
      return (
        <ButtonsContainer>
          <Btn color='#777' onClick={this._onConn}>Connect</Btn>
        </ButtonsContainer>
      );
    }
    return (
      <ButtonsContainer>
        <Btn color='#d22' onClick={this._onDisconnect}>Disconnect</Btn>
        <Btn color='#777' onClick={this._onCal}>Calibrate</Btn>
        <Btn color='#191' onClick={this._onScan}>Scan</Btn>
      </ButtonsContainer>
    );
  }

  render() {
    const {device, colorScan} = this.props;
    if (!device){
      return this._renderNoneSelected();
    }
    const { serial, rssi, connectionStatus, fw, isCalibrated, lastCalScanCount, scanCount, deviceType } = device;
    const batt = device.battery ? device.battery.level : 0.0;

    return (
      <MainContent>
        <DeviceInfoHeader
          serial={serial}
          rssi={rssi}
          connectionStatus={connectionStatus}
          batteryLevel={batt}
          fw={fw}
          isCalibrated={isCalibrated}
          lastCalScanCount={lastCalScanCount}
          scanCount={scanCount}
          deviceType={deviceType}
        />
        <ColorDetailView colorScan={colorScan} />
        {this._renderBtns(connectionStatus)}
      </MainContent>
    );
  }
}
