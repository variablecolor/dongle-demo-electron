import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
  width: 100%;
  margin-bottom: 10px;
  border-bottom-color: #ccc;
  border-bottom-width: 1px;
  border-bottom-style: solid;
  display: flex;
  flex-direction: column;
  
`;


const HeaderText = styled.h2`
  color: #ccc;
  text-align: center;
  margin: 10px;
`;

const DevInfoRow = styled.div`
  height: 18px;
  width: 100%;
  margin-bottom: 4px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const RowTitle = styled.div`
  flex: 1;
  color: #ccc;
  font-size: 14px;
  font-weight: bold;
  text-align: right
  margin-right: 8px;
`;
const RowDetail = styled.div`
  flex: 1;
  color: #ccc;
  font-size: 14px;
  text-align: left
  margin-left: 8px;
`;

type Props = {
  serial: string,
  rssi: number,
  connectionStatus: string, // DISCONNECTED, DISCOVERED, CONNECTED
  batteryLevel: number,
  fw: string, // "60.23",
  // handle: number, // 1,
  isCalibrated: boolean,
  lastCalScanCount: number, // 1695
  scanCount: number, // 1695
};
export default class DeviceInfoHeader extends React.PureComponent<Props, any>{

  _renderDisconnected = (headerText: string) => {
    console.log('disconnected - render dev info');
    const {connectionStatus} = this.props;
    return (
      <Container>
        <HeaderText>{headerText}</HeaderText>
        <DevInfoRow>
          <RowTitle>Connection Status</RowTitle>
          <RowDetail>{connectionStatus}</RowDetail>
        </DevInfoRow>
      </Container>
    );
  }

  render() {
    const {
      serial,
      rssi,
      connectionStatus, // DISCONNECTED, DISCOVERED, CONNECTED
      batteryLevel,
      fw, // "60.23",
      // handle, // 1,
      isCalibrated,
      lastCalScanCount, // 1695
      scanCount,
      deviceType,
    } = this.props;
    const dispData = {
      'Bluetooth Status': connectionStatus,
      RSSI: rssi,
      Battery: `${batteryLevel}%`,
      'FW Version': fw,
      'Last Calibrated Scan #': lastCalScanCount,
      'Scan Count (Lifetime)': scanCount,
      'Device Needs Calibration': isCalibrated ? 'NO' : 'YES',
    };
    const keys = Object.keys(dispData);
    const headerText = deviceType === 'spectro' ? `Spectro 1 - ${serial} - Info` : `Muse Pro - ${serial} - Info`;

    if (connectionStatus !== 'CONNECTED'){
      return this._renderDisconnected(headerText);
    }

    return (
      <Container>
        <HeaderText>{headerText}</HeaderText>
        {keys.map(key => (
          <DevInfoRow key={`dev-info-row-${key}`}>
            <RowTitle>{key}</RowTitle>
            <RowDetail>{dispData[key]}</RowDetail>
          </DevInfoRow>
        ))}
      </Container>
    );
  }
}
