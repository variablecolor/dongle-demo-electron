import React from 'react';
import styled from 'styled-components';

import { ColorScan } from '../../model';
import SpectralGraph from './SpectralGraph';

const Container = styled.div`
  width: 100%;
  padding: 10px;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;


const ChartsContainer = styled.div`
  width: 100%;
  min-height: 80px;
  padding: 10px;
  display: flex;
  flex-direction: row;
  box-sizing: border-box;
`;

const ColorComponentContainer = styled.div`
  flex: 1;
  margin: 10px;
  
  box-sizing: border-box;

  border-bottom-color: #f00;
  border-bottom-width: 1px;
  border-bottom-style: solid;
`;

const ColorPreview = styled.div`
  height: 100%;
  width: 100%;
  background-color: ${props => props.color};
`;

const ColorInfoRow = styled.div`
  height: 24px;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;
const RowTitle = styled.div`
  flex: 1;
  color: #ccc;
  font-size: 14px;
  font-weight: bold;
  text-align: center;
`;
const RowDetail = styled.div`
  flex: 1;
  color: #ccc;
  font-size: 14px;
  text-align: center;
`;


type Props = {
  colorScan: ColorScan,
}
export default class ColorDetailView extends React.Component<Props, any>{


  render() {
    const { colorScan} = this.props;
    if (!colorScan) {
      return null;
    }
    const {spectrum, hex, lab} = colorScan;

    if (!hex || !lab){
      console.log('no color data');
      return null;
    }
    const hasSpectrum = (spectrum !== undefined);

    return (
      <Container>
        <ChartsContainer>
          {hasSpectrum && (
          <ColorComponentContainer>
            <SpectralGraph spectra={[spectrum]} />
          </ColorComponentContainer>
          )}
          <ColorComponentContainer>
            <ColorPreview color={hex} />
          </ColorComponentContainer>
        </ChartsContainer>
        <ColorInfoRow>
          <RowTitle>HEX</RowTitle>
          <RowDetail>{hex.toUpperCase()}</RowDetail>
        </ColorInfoRow>
        <ColorInfoRow>
          <RowTitle>{`CIE Lab ${lab.illuminant}`}</RowTitle>
          <RowDetail>{`${lab.L.toFixed(2)}, ${lab.a.toFixed(2)}, ${lab.b.toFixed(2)}`}</RowDetail>
        </ColorInfoRow>
      </Container>
    );

  }
}
