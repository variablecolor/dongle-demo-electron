
import React from 'react';
import { Scatter, defaults, Chart } from 'react-chartjs-2';
import styled from 'styled-components';
import {Spectrum} from '../../model';

defaults.global.animation = false;
defaults.global.fontFamily = 'Montserrat';


const LoadingContainer = styled.div`
  width: 100%;
`;

type Props = {
  spectra: Array<Spectrum>,
};

type ViewState = {
  plotData: Chart.ChartData
}

export default class SpectralGraph extends React.Component<Props, ViewState>{
  constructor(props: Props){
    super(props);
    this.state = {
      plotData: undefined,
    };
  }

  componentDidMount = () => {
    this._calculateGraphData();
  }

  componentWillReceiveProps = (nextProps) => {
    this._calculateGraphData(nextProps);
  }

  get chartOptions(): Chart.ChartOptions {
    const {spectra} = this.props;
    let minX = 5000;
    let maxX = 100;
    spectra.forEach((s) => {
      if (s.start < minX){
        minX = s.start;
      }
      if (s.start + s.step * s.curve.length > maxX){
        maxX = s.start + s.step * s.curve.length;
      }
    });

    const options: Chart.ChartOptions = {
      responsive: true,
      legend: { display: false },
      showLines: true,
      pointDot: false,
      datasetFill: false,
      scales: {
        xAxes: [{
          ticks: {
            suggestedMin: minX,
            suggestedMax: maxX,
          },
          scaleLabel: {
            display: true,
            labelString: 'Wavelength (nm)',
          },
        }],
        yAxes: [{
          ticks: {
            suggestedMin: 0.0,
            suggestedMax: 1.0,
          },
          scaleLabel: {
            display: true,
            labelString: 'Reflectance %',
          },
        }],
      },
    };
    return options;
  }

  _calculateGraphData = async (props) => {
    const {spectra} = (props !== undefined) ? props : this.props;
    if (!spectra || spectra.length === 0){
      return;
    }
    const spectrum = spectra[0];
    const {start, step, curve} = spectrum;
    const labels = [];
    const points = [];
    const stdDataset = {
      fill: false,
      showLine: true,
      pointBorderColor: '#fe2211', //hex,
      pointBackgroundColor: '#fe2211', //hex,
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#fff',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      lineTension: 0.1,
      borderColor: '#fe2211', //hex,
    };
    for (let i = 0; i < curve.length; i++){
      const nm = start + i * step;
      labels.push(nm.toString());
      points.push({x: nm, y: curve[i]});
    }
    const data: Chart.ChartData = {
      labels,
      datasets: [
        {
          ...stdDataset,
          label: '',
          data: points,
        },
      ],
    };
    this.setState({
      plotData: data,
    });
  }

  render() {
    const {spectra} = this.props;
    const legendOpts = { display: false};
    if ((spectra === undefined) || (spectra.length === 0)){
      return null;
    }

    const { plotData } = this.state;
    if (!plotData){
      return <LoadingContainer>Constructing spectrum graph</LoadingContainer>;
    }

    return (
      <Scatter data={plotData} legend={legendOpts} options={this.chartOptions} />
    );
  }
}
