import React from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';
import { ColorScan } from '../../model';
import dongleIO, {DongleIOListener} from '../../dongle/DongleIO';
import { modalStyle, ButtonsContainer, Btn, Header, HeaderText } from '../common';

const Container = styled.div`
  position: absolute;
  top: 50px;
  left: 180px;
  bottom: 0;
  right: 0;

  box-sizing: border-box;
  padding: 10px;
  border-left-color: #111;
  border-left-width: 2px;
  border-left-style: solid;
`;

const TopContainer = styled.div`
  height: 100%
  width: 100%;
  position: relative;
  box-sizing: border-box;
`;


const CalRows = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

const CalRow = styled.div`
  height: 60px;
  width: 100%;
  margin: 10px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const Preview = styled.div`
  height: 50px;
  width: 50px;
  margin: auto;
  background-color: ${props => (props.color ? props.color : '#111')};
  border-color: #ccc
  border-width: 1px;
  border-radius: 30px;
  border-style: solid;
`;


type Props = {
  serial: string,
  deviceType: string,
  onDismiss: () => void,
}
type State = {
  whiteScan: ColorScan,
  greenScan: ColorScan,
  blueScan: ColorScan,
  currentAction: string,
  showModal: boolean,
  modalText: string,
}
export default class CalibrationView extends React.Component<Props, State> implements DongleIOListener{
  constructor(props: Props){
    super(props);
    this.state = {
      whiteScan: undefined,
      greenScan: undefined,
      blueScan: undefined,
      currentAction: undefined,
      showModal: false,
      modalText: undefined,
    };
  }

  componentDidMount = () => {
    dongleIO.dongleListener = this;
  }

  //DongleIO Listener:
  //  -> Very basic just handling calibration i/o and related errors for now:
  onDongleUpdate = () => {}
  onDevicesUpdate = () => {}
  onLicenseSucecss = () => {}
  onColorScan = (serial: string, scan: ColorScan) => {
    const { currentAction } = this.state;
    switch (currentAction) {
      case 'white':
        this.setState({whiteScan: scan, currentAction: undefined});
        break;
      case 'green':
        this.setState({greenScan: scan, currentAction: undefined});
        break;
      case 'blue':
        this.setState({blueScan: scan, currentAction: undefined});
        break;
      default:
        break;
    }
  }
  onError = (error: Error) => {
    console.log('Error from Dongle: ', error);
    this.setState({showModal: true, modalText: `Error: ${error.message}`});
  }

  onCalSuccess = () => {
    console.log('onCalSuccess');
    this.setState({showModal: true, modalText: 'Calibration Succeeded!'});
  }

  //Click Handlers:

  _onScanWhite = () => {
    const {serial} = this.props;
    dongleIO.scanColor(serial);
    this.setState({currentAction: 'white'});
  }
  _onScanGreen = () => {
    const {serial} = this.props;
    dongleIO.scanColor(serial);
    this.setState({currentAction: 'green'});
  }
  _onScanBlue = () => {
    const {serial} = this.props;
    dongleIO.scanColor(serial);
    this.setState({currentAction: 'blue'});
  }
  _onSetCalibration = () => {
    const {serial, deviceType} = this.props;
    const {whiteScan: ws, greenScan: gs, blueScan: bs } = this.state;
    if (deviceType === 'spectro'){
      if (!ws || !gs || !bs || !ws.senseValues || !gs.senseValues || !bs.senseValues) {
        this.setState({showModal: true, modalText: 'Error: Cant set calibration without all 3 scans'});
        return;
      }
      dongleIO.setCalibration(serial, ws.senseValues, gs.senseValues, bs.senseValues);
    } else if (deviceType === 'color muse pro') {
      if (!ws || !ws.senseValues ) {
        this.setState({showModal: true, modalText: 'Error: Cant set calibration without tile scan'});
        return;
      }
      dongleIO.setCalibration(serial, ws.senseValues, [], []);
    }

    this.setState({currentAction: 'set'});
  }
  _onCloseModal = () => {
    this.setState({showModal: false, modalText: ''});
  }

  _renderSpectroCalibration = () => {
    const {whiteScan, greenScan, blueScan } = this.state;
    const whiteHex = whiteScan ? whiteScan.hex : undefined;
    const greenHex = greenScan ? greenScan.hex : undefined;
    const blueHex = blueScan ? blueScan.hex : undefined;

    return (
      <TopContainer>
        <Header>
          <HeaderText>Spectro 1 Calibration</HeaderText>
        </Header>
        <CalRows>
          <CalRow>
            <Preview color={whiteHex} />
            <Btn color='#777' onClick={this._onScanWhite}>Scan White Tile</Btn>
          </CalRow>
          <CalRow>
            <Preview color={greenHex} />
            <Btn color='#777' onClick={this._onScanGreen}>Scan Green Tile</Btn>
          </CalRow>
          <CalRow>
            <Preview color={blueHex} />
            <Btn color='#777' onClick={this._onScanBlue}>Scan Blue Tile</Btn>
          </CalRow>
        </CalRows>
      </TopContainer>
    );
  }

  _renderCMPCalibration = () => {
    const {whiteScan } = this.state;
    const whiteHex = whiteScan ? whiteScan.hex : undefined;

    return (
      <TopContainer>
        <Header>
          <HeaderText>ColorMusePro Calibration</HeaderText>
        </Header>
        <CalRows>
          <CalRow>
            <Preview color={whiteHex} />
            <Btn color='#777' onClick={this._onScanWhite}>Scan White Tile</Btn>
          </CalRow>
        </CalRows>
      </TopContainer>
    );
  }

  render(){
    const {onDismiss, deviceType} = this.props;
    const {showModal, modalText } = this.state;


    return (
      <Container>
        {(deviceType === 'spectro') ? this._renderSpectroCalibration() : this._renderCMPCalibration()}

        <Modal
          isOpen={showModal}
          style={modalStyle}
          ariaHideApp={false}
        >
          <Header>
            <HeaderText>{modalText}</HeaderText>
          </Header>
          <ButtonsContainer>
            <Btn color='#777' onClick={this._onCloseModal}>OK</Btn>
          </ButtonsContainer>
        </Modal>

        <ButtonsContainer>
          <Btn color='#d22' onClick={onDismiss}>Dismiss</Btn>
          <Btn color='#191' onClick={this._onSetCalibration}>Set Calibration</Btn>
        </ButtonsContainer>
      </Container>
    );
  }
}
