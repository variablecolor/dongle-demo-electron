import styled from 'styled-components';


const modalStyle = {
  overlay: {
    backgroundColor: 'rgba(180, 180, 180, 0.75)',
  },
  content: {
    top: '30%',
    left: '20%',
    right: '20%',
    height: '30%',
    padding: 0,
    paddingTop: 10,
    backgroundColor: '#333',
    boxSizing: 'border-box',
  },
};

const Header = styled.div`
  width: 100%;
  height: 30%;
  margin-bottom: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const HeaderText = styled.h2`
  color: #ccc;
  text-align: center;
  margin: 10px;
`;

const ButtonsContainer = styled.div`
  position: absolute;
  left: 0px;
  width: 100%;
  height: 60px;
  bottom: 0px;
  padding: 10px;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Btn = styled.div`
  height: 100%;
  flex: 1;
  margin: 10px;
  box-sizing: border-box;
  color: #ccc;
  border-color: ${props => props.color}
  border-width: 1px;
  border-radius: 30px;
  border-style: solid;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  ::selection { background: transparent; }
  ::-moz-selection { background: transparent; }

  :hover {
    background-color: ${props => props.color}
  }
  :active {
    background-color: #444;
  }
`;

export {modalStyle, ButtonsContainer, Btn, Header, HeaderText};
