import React from 'react';
import styled from 'styled-components';
import {SocketState} from '../dongle/DongleIO';

const Container = styled.div`
  height: 50px;
  width: 100%;
  background-color: #222;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

const Column = styled.div`
  height: 100%;
  flex: 2;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
const FWColumn = styled.div`
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Title = styled.div`
  color: #ccc;
  font-weight: bold;
  font-size: 18px;
  text-align: right;
  margin-right: 8px;
`;

const Status = styled.div`
  color: ${props => props.color};
  font-weight: bold;
  font-size: 18px;
  text-align: left;
  margin-left: 8px;
`;

type Props = {
  serverStatus: string,
  usbStatus: string,
  dfw: ?string,
}
export default class HeaderView extends React.PureComponent<Props, any>{


  render(){
    const {serverStatus, usbStatus, dfw} = this.props;
    console.log('serverStatus: ', serverStatus, ', usbStatus: ', usbStatus);
    const serverColor = (serverStatus === SocketState.CONNECTED) ? 'green' : 'red';
    const serverString = (serverStatus === SocketState.CONNECTED) ? 'CONNECTED' : 'DISCONNECTED';
    const usbColor = (usbStatus.toLowerCase() === 'connected') ? 'green' : 'red';
    const showDFW = (dfw !== undefined);
    const fwstr = `FW: ${dfw}`;

    return (
      <Container>
        <Column>
          <Title>Dongle Server Status</Title>
          <Status color={serverColor}>{serverString}</Status>
        </Column>
        <Column>
          <Title>USB Status</Title>
          <Status color={usbColor}>{usbStatus.toUpperCase()}</Status>
        </Column>
        {showDFW && (
          <FWColumn>
            <Title>{fwstr}</Title>
          </FWColumn>
        )}

      </Container>
    );

  }
}
