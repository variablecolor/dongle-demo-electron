import React from 'react';
import styled from 'styled-components';

import dongleIO, {DongleIOListener, SocketState} from '../dongle/DongleIO';

import { Spectrum, Spectro, ColorScan, Lab } from '../model';
import HeaderView from './HeaderView';
import LeftBar from './left-nav/LeftBar';
import DetailPane from './detail-pane/DetailPane';
import CalibrationView from './detail-pane/CalibrationView';

const {dialog} = require('electron').remote;
const ipc = require('electron').ipcRenderer;


const AlterType = 'warning' | 'info' | 'error';
const Alert = (title: string, msg: string, type: AlterType) => {
  const windowTitle = (type === 'warning') ? 'Warning' : (type === 'error') ? 'Error' : 'Info';
  const options = {
    type,
    noLink: true,
    title: windowTitle,
    message: title,
    detail: msg,
    buttons: ['OK'],
  };
  dialog.showMessageBox(options);
};

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  background-color: #333;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

const Body = styled.div`
  flex: 1;
  width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
`;

type State = {|
  devices: Array<Spectro>,
  selectedSerial: string,
  currentScan: ColorScan,
  isShowingCalibration: boolean,
  serverStatus: number,
  usbStatus: string,
  dongleFw: ?string,
|};

export default class MainView extends React.Component<any, State> implements DongleIOListener{
  constructor(props) {
    super(props);
    this.state = {
      devices: [],
      selectedSerial: undefined,
      currentScan: undefined,
      isShowingCalibration: false,
      serverStatus: SocketState.DISCONNECTED,
      usbStatus: 'disconnected',
      dongleFw: undefined,
    };
  }

  componentDidMount = () => {
    dongleIO.dongleListener = this;
    dongleIO.connectToDongleServer();
    ipc.on('delete-all-serials', () => {
      dongleIO.removeAllLicenses();
    });
    ipc.on('request-server-shutdown', () => {
      dongleIO.shutdownServer();
    });
  }

  _deviceSelected = (serial: string) => {
    const {selectedSerial} = this.state;
    if (serial !== selectedSerial){
      this.setState({selectedSerial: serial, currentScan: undefined});
    }
  }


  //DongleIO Listener:

  onDongleUpdate = () => {
    //dongle connected / dongle disconnected
    const {socketState: serverStatus, status: usbStatus} = dongleIO;
    this.setState({serverStatus, usbStatus});
  }
  onDevicesUpdate = () => {
    //device connected / disconnected / was licensed / license removed / etc
    //also used for on battery/calibration updated as this affects spectro objects too

    const { devices, status: usbStatus, dongleFw } = dongleIO;
    this.setState({devices: [...devices], usbStatus, dongleFw});
  }
  onColorScan = (serial: string, scan: ColorScan) => {
    const { selectedSerial } = this.state;
    if (selectedSerial !== serial){
      console.log('Got Scan for another device - ignoring');
      return;
    }
    this.setState({currentScan: scan});
  }
  onError = (error: Error) => {
    console.log('Error from Dongle: ', error);
    const {socketState: serverStatus, status: usbStatus} = dongleIO;
    this.setState({serverStatus, usbStatus}, () => {
      Alert('Dongle Server Error', error.message, 'error');
    });
  }

  // Ignore Calibration here - handled in CalibrationView
  onCalSuccess = () => {
    console.log('Got unexpected onCalSuccess');
  }
  onLicenseSucecss = () => {
    Alert('Success!', 'Licenses updated, refreshing...', 'info');
    dongleIO.refreshDongleInfo();
  }

  //Click Handlers:

  _onScanClick = (serial: string) => {
    console.log('request scan for: ', serial);
    dongleIO.scanColor(serial);

    if (0){
      //for test
      const rSpectrum = Spectrum.random();
      const rHex = `#${(Math.random() * 0xFFFFFF << 0).toString(16)}`;
      const scan = new ColorScan();
      const lab = Lab.fromJSON({l: 23, a: 33, b: 45, illuminant: 'D65'});
      scan.spectrum = rSpectrum;
      scan.hex = rHex;
      scan.lab = lab;
      console.log('new hex: ', rHex);
      this.setState({currentScan: scan});
    }
  }
  _onCalClick = (serial: string) => {
    console.log('request cal for: ', serial);
    this.setState({isShowingCalibration: true});
  }
  _onConnectClick = (serial: string) => {
    console.log('request connect for: ', serial);
    dongleIO.connectSpectro(serial);
  }
  _onDisconnectClick = (serial: string) => {
    console.log('request disconnect for: ', serial);
    dongleIO.disconnectSpectro(serial);
  }

  _onDismissCalibration = () => {
    this.setState({isShowingCalibration: false});
    dongleIO.dongleListener = this;
    dongleIO.refreshDongleInfo(); //update device needs calibration & counts
  }


  _onAddLicense = () => {
    console.log('add license...');
    dialog.showOpenDialog({properties: ['openFile']}, (files) => {
      if (files !== undefined && files.length > 0) {
        // handle files, since multiselect isn't set - we'll only get one
        const path = files[0];
        console.log('selected file: ', path);
        dongleIO.installLicense(path, false /* overwrite for 'adv' users only */);
      }
    });
  }

  _onDeleteLicense = (serial: string) => {
    console.log('MainView - Deleting license: ', serial);
    dongleIO.removeLicense(serial);
  }

  render() {
    const { devices, selectedSerial, currentScan, isShowingCalibration, dongleFw } = this.state;
    const {serverStatus, usbStatus} = this.state;
    const idx = devices.findIndex(ds => ds.serial === selectedSerial);
    const selectedDevice = (idx >= 0) ? devices[idx] : undefined;
    const deviceType = (idx >= 0) ? selectedDevice.deviceType : undefined;

    if (isShowingCalibration){
      return (
        <Container>
          <HeaderView serverStatus={serverStatus} usbStatus={usbStatus} dfw={dongleFw} />
          <Body>
            <LeftBar devices={devices} onDeviceSelected={this._deviceSelected} selectedSerial={selectedSerial} />
            <CalibrationView serial={selectedSerial} onDismiss={this._onDismissCalibration} deviceType={deviceType} />
          </Body>
        </Container>
      );
    }
    return (
      <Container>
        <HeaderView serverStatus={serverStatus} usbStatus={usbStatus} dfw={dongleFw}  />
        <Body>
          <LeftBar
            devices={devices}
            onDeviceSelected={this._deviceSelected}
            selectedSerial={selectedSerial}
            onAddLicense={this._onAddLicense}
            onDeleteLicense={this._onDeleteLicense}
          />
          <DetailPane
            device={selectedDevice}
            colorScan={currentScan}
            onScanClick={this._onScanClick}
            onCalClick={this._onCalClick}
            onConnectClick={this._onConnectClick}
            onDisconnectClick={this._onDisconnectClick}
          />
        </Body>
      </Container>
    );
  }
}
