import React from 'react';
import styled from 'styled-components';

import { Spectro } from '../../model';
import DeviceSelector from './DeviceSelector';


const Container = styled.div`
  position: absolute;
  left: 0;
  top: 50px;
  bottom: 0;
  width: 180px;

  background-color: #222;
`;

const DeviceList = styled.div`
  height: 100%;
  width: 100%;
  overflow-y: scroll;
`;

const HeaderContainer = styled.div`
  width: 100%;
  height: 30px;
  background-color: #333;
  box-sizing: border-box;
  padding: 10px;
  font-size: 14px
  font-weight: bold;
  text-align: center;
  color: white;
`;


const AddDeviceContainer = styled.div`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 60px;
  background-color: #333;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  cursor: pointer;

  :active {
    background-color: #444;
  }
`;

const AddBtn = styled.div`
  margin: auto;
  height: 36px;
  width: 36px;
  border-radius: 18px;
  border-color: #ccc;
  border-width: 1px;
  border-style: solid;
  background-color: #191;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

const Plus = styled.div`
  font-size: 24px;
  font-weight: bold;
  color: #ccc;
  cursor: pointer;
  ::selection { background: transparent; }
  ::-moz-selection { background: transparent; }
`;


type Props = {
  devices: Array<Spectro>,
  onDeviceSelected: (serial: string) => void,
  onAddLicense: () => void,
  onDeleteLicense: (serial: string) => void,
  selectedSerial: string,
}
export default class LeftBar extends React.Component<Props, any> {

  render() {
    const { devices, onDeviceSelected, selectedSerial: selSer, onAddLicense, onDeleteLicense } = this.props;

    return (
      <Container>
        <HeaderContainer>Licensed Devices</HeaderContainer>
        <DeviceList>
          {devices.map((device) => {
            const k = `device-selector-${device.serial}`;
            return (
              <DeviceSelector
                key={k}
                device={device}
                onClick={onDeviceSelected}
                onDelete={onDeleteLicense}
                selected={selSer === device.serial}
              />
            );
          })}
        </DeviceList>
        <AddDeviceContainer onClick={onAddLicense}>
          <AddBtn><Plus>+</Plus></AddBtn>
        </AddDeviceContainer>
      </Container>
    );
  }
}
