import React from 'react';
import styled from 'styled-components';

import { Spectro } from '../../model';

const { Menu, MenuItem, getCurrentWindow} = require('electron').remote;


const Container = styled.div`
  width: 100%;
  height: 60px;
  padding: 10px;
  margin-bottom: 1px;
  background-color: ${props => (props.selected ? '#444' : '#222')};
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  cursor: pointer;
`;

const Serial = styled.div`
  font-size: 16px;
  color: #ccc;
  flex: 1;
`;

const Status = styled.div`
  height: 10px;
  width: 10px;
  border-color: #ccc;
  border-radius: 5px;
  border-width: 2px;
  border-style: solid;
  box-sizing: border-box;
  background-color: ${props => props.color};
`;


type Props = {
  device: Spectro,
  onClick: (serial: string) => void,
  onDelete: (serial: string) => void,
  selected: boolean
}
export default class DeviceSelector extends React.Component<Props, any> {
  menu: any;
  containerId: string;

  constructor(props: Props){
    super(props);
    this.menu = new Menu();
    const menuItem = new MenuItem({
      label: 'Delete',
      click: this._onDelete,
    });
    this.menu.append(menuItem);
    this.containerId = `device-selector-${props.device.serial}`;

  }
  componentDidMount = () => {
    document.getElementById(this.containerId).onmousedown = (event) => {
      if (event.which === 3) {
        this._onRightClick();
      }
    };
  }


  _onClick = () => {
    const { device, onClick } = this.props;
    onClick(device.serial);
  }

  _onDelete = () => {
    const {device, onDelete} = this.props;
    console.log('Deleting: ', device.serial);
    onDelete(device.serial);
  }

  _onRightClick = () => {
    const {device} = this.props;
    console.log('_onRightClick: ', device.serial);
    this.menu.popup(getCurrentWindow());
    return false;
  }

  render() {
    const { device, selected } = this.props;
    let color = 'red';
    if (device.connectionStatus === 'DISCOVERED') {
      color = 'orange';
    } else if (device.connectionStatus === 'CONNECTING') {
      color = 'yellow';
    } else if (device.connectionStatus === 'CONNECTED') {
      color = 'green';
    }

    return (
      <Container id={this.containerId} onClick={this._onClick} selected={selected}>
        <Serial>{device.serial}</Serial>
        <Status color={color} />
      </Container>
    );
  }
}
