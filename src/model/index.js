import Spectrum from './Spectrum';
import Spectro from './Spectro';
import ColorScan, { Lab } from './ColorScan';

export { Spectrum, Spectro, ColorScan, Lab };
