
class Battery {
  // "battery": {
  //   "is_charging": true,
  //   "is_charging_complete": false,
  //   "level": 100,
  //   "voltage": 4.249
  // }
  isCharging: boolean;
  isChargeComplete: boolean;
  level: number; // 100,
  voltage: number; // 4.249

  static fromJSON = (json) => {
    const { is_charging: isCharging, is_charging_complete: isChargeComplete, level, voltage } = json;
    return Object.assign(new Battery(), { isCharging, isChargeComplete, level, voltage });
  }

}

export default class Spectro {
  serial: string;
  rssi: number;
  connectionStatus: string; // DISCONNECTED, DISCOVERED, CONNECTING, CONNECTED

  battery: Battery;
  deviceType: string; // "spectro" / "color muse pro"
  fw: string; // "60.23",
  handle: number; // 1,
  specularComponent: string;

  // spectro specific properties:
  isCalibrated: boolean;
  lastCalScanCount: number; // 1695
  scanCount: number; // 1695

  static fromJSON = (json): Spectro => {
    const battery = Battery.fromJSON(json.battery);
    const { spectro } = json;
    const {
      is_calibrated: isCalibrated,
      last_calibration_scan_count: lastCalScanCount,
      lifetime_scan_count: scanCount,
    } = spectro;
    const { device_type: deviceType, firmware_version: fw, handle, serial, rssi } = json;
    const obj = { deviceType, fw, handle, isCalibrated, lastCalScanCount, scanCount, serial, battery, rssi };
    const s = Object.assign(new Spectro(), obj);
    return s;
  }

}
