

class Spectrum {
  start: number;
  step: number;
  curve: number[];

  static fromJSON = (json) => {
    const s = Object.assign(new Spectrum(), json);
    return s;
  }

  static random = () => (
    Object.assign(new Spectrum(), ({
      start: 400,
      step: 10,
      curve: Spectrum.randomSpectralCurve(31),
    }))
  );

  static randomSpectralCurve = (count) => {
    const curve = [];
    let val = 0;
    for (let i = 0; i < count; i++){
      if (i % 2 === 0){
        val = (0.35 * Math.random());
      } else {
        val = 0.95 * Math.random();
      }
      curve.push(Number(val.toFixed(5)));
    }
    return curve;
  };
}

export default Spectrum;
