import Spectrum from './Spectrum';

export class Lab {
  L: number;
  a: number;
  b: number;
  illuminant: string;
  observer: string;

  static fromJSON = (json) => {
    if (json.d50L){
      return Lab.fromLegacyJSON(json);
    }
    const lab = Object.assign(new Lab(), json);
    return lab;
  }
  static fromLegacyJSON = (json) => {
    const {d50L: L, d50a: a, d50b: b, illuminant, observer} = json;
    const lab = Object.assign(new Lab(), {L, a, b, illuminant, observer});
    return lab;
  }
}

export default class ColorScan {
  spectrum: Spectrum;
  hex: string;
  lab: Lab;
  senseValues: Array<number>;
}
