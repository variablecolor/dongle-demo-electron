require('dotenv').config()
const packageJson = require('./package.json')

const config = {
  "packageConfig":{
    "osxSign": {
      "identity": "Developer ID Application: Variable Technologies, LLC (54YDEGU5K7)",
      "hardened-runtime": true,
      "entitlements": "entitlements.plist",
      "entitlements-inherit": "entitlements.plist",
      "signature-flags": "library"
    },
    "osxNotarize": {
      "appleId": null,
      "appleIdPassword": null,
    },
    "appBundleId": "com.variabletech.bridge",
  },
  "makers": [
    {
      "name": "@electron-forge/maker-squirrel",
      "config": {
        "name": "test_ef6"
      }
    },
    {
      "name": "@electron-forge/maker-zip",
      "platforms": [
        "darwin"
      ]
    },
    {
      "name": "@electron-forge/maker-deb",
      "config": {}
    },
    {
      "name": "@electron-forge/maker-rpm",
      "config": {}
    }
  ],
  "plugins": [
    [
      "@electron-forge/plugin-webpack",
      {
        "mainConfig": "./webpack.main.config.js",
        "renderer": {
          "config": "./webpack.renderer.config.js",
          "entryPoints": [
            {
              "html": "./src/index.html",
              "js": "./src/app.js",
              "name": "main_window"
            }
          ]
        }
      }
    ]
  ]
}



function notarizeMaybe() {
 

  if (process.platform !== 'darwin') {
    return;
  }

  if (process.env.npm_lifecycle_event !== 'publish') {
    console.log(`\nNot in "publish" mode, skipping notarization`);
    return;
  }

  if (!process.env.APPLE_ID || !process.env.APPLE_ID_PASSWORD) {
    throw new Error('Should be notarizing, but environment variables APPLE_ID or APPLE_ID_PASSWORD are missing!');
  }

  config.packageConfig.osxNotarize.appleId = process.env.APPLE_ID
  config.packageConfig.osxNotarize.appleIdPassword = process.env.APPLE_ID_PASSWORD
}

notarizeMaybe()

module.exports = { config }