# Variable Dongle Demo - Electron #

 - See downloads for precompiled binaries

Welcome to the Dongle Demo - Electronjs version. 
This project is a basic demo on using the Variable USB Dongle hardware & Dongle Server (dongle-server service). 

As the Dongle Server (service/software) is available in OSX, Windows, & Linux - Electron was chosen as it's also cross-platform. 

Communcation with the dongle-server service is done via TCP socket communication...



## Dongle Demo Features ##

This demo shows how to: 

* Invoke the dongle-server service
* Open a TCP Socket to the dongle-server service
* Issue commands to:

    * Get status on the USB Dongle Hardware
    * Install a license for a given Spectro 1
    * Connect (Bluetooth) to Spectro 1
    * Issue commands / get information on the connected Spectro 1(s)

* Handle errors from the dongle-server service


#### Demonstrating The Dongle-Server API ####

Most of the business logic dealing with the Dongle communications can be found in the `src/dongle/` folder:

* `src/dongle/DongleIO.js` demonstrates all dongle API command and data I/O
* `src/dongle/DongleProcess.js` demonstrates starting the dongle-server service.


## Prerequesites ##

In order to run the dongle software you will need: 

* USB Dongle Hardware from Variable, Inc
* Spectro 1 device
* A dongle specific license file for the Spectro 1 device

If you do not have any of the above, contact your Variable, Inc representative. 


## Getting Started - OSX / MacOS ##

This is merely providing one way to perform the general step-by-step instructions above on OSX. 
This assumes you have [Homebrew](https://brew.sh/) installed. 


1. Download & Install OSX [drivers for the USB Dongle](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)

2. Install NodeJS & NPM: 

        #!bash
        $ brew install node

3. Install [electron-forge](https://electronforge.io/):

        #!bash
        $ npm i -g @electron-forge/cli

4. Download project and unzip to somewhere on your machine... such as `~/code/js/electron/`. So the extracted project would then have the following base folder: `~/code/js/electron/dongle-demo-electron/`
5. Navigate to the base folder, install dependencies, & run:

        #!bash
        $ cd ~/code/js/electron/dongle-demo-electron/
        $ npm install
        $ npm start

6. At this point - the app should be up and running and you can test the various features: 
    * Installing a license (By tapping the **`+`** on the bottom left of the UI)
    * General Spectro 1 usage such as: scan, dev info, calibration (By tapping on the serial # of your **`Licensed Device`** on the left of the screen)


## Getting Started - Windows ##

The process for getting started in Windows (Tested on Windows 10) depends on having a shell application. This demo was tested, debugged, and packaged using [Git Bash](https://gitforwindows.org/) . 

Alternative "install npm for windows" reference [here](https://gist.github.com/changtimwu/4117025).

1. Download & Install OSX [drivers for the USB Dongle](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)
2. Download & Install [Git Bash](https://gitforwindows.org/)
3. Download & Install [NodeJS & npm Windows Installer](https://nodejs.org/en/download/)

    * Confirm install: 

            #!bash
            $ node -v
            v10.15.3
            $ npm -v
            6.4.1

4. Install [electron-forge](https://electronforge.io/):

        #!bash
        $ npm i -g @electron-forge/cli

5. Download project and unzip to somewhere on your machine... such as `C:\Users\youraccount\code\js\electron\`. So the extracted project would then have the following base folder in Git Bash: `~/code/js/electron/dongle-demo-electron/`
6. In `Git Bash`, navigate to the base folder, install dependencies, & run:

        #!bash
        $ cd ~/code/js/electron/dongle-demo-electron/
        $ npm install
        $ npm start

7. At this point - the app should be up and running and you can test the various features: 
    * Installing a license (By tapping the **`+`** on the bottom left of the UI)
    * General Spectro 1 usage such as: scan, dev info, calibration (By tapping on the serial # of your **`Licensed Device`** on the left of the screen)


## Deployment ##


Deployment is easiest to do from the native environment the app is intended for (so run the follow from Windows for Windows deployment, and OSX for OSX). 

1. Package: 

        #!bash
        $ npm run package

*    Executable files will be compiled and placed in the `~/code/js/electron/dongle-demo-electron/out/` folder.



## Important Note ##

*  *On MacOS / OSX, running `electron-forge` in debug mode with `electron-forge start` will not include the AppName, version, and About information. To see these pieces, package the app (as described in the previous section) and run the packaged app to see version, appname, etc.*

*  *On MacOS / OSX - App Version can be seen in the dropdown menu for OSX (native to electron).*

*  *On Windows - App Version can be seen by right clicking on the packaged executable, and clicking the Details tab.*


## Not Implemented ##

The following have yet to be implemented or aren't planned:

1. Creating installer / MSI --TBD
2. Resizeable app window. --TBD
3. UI Feedback for long processes (like a spinner while scanning)


## Authors / Contact

* **Andrew Temple** - _Initial work - Electron/JS_
* **Corey Mann** - _Initial work - C# - Dongle_
* Email dev@variableinc.com

## License

This project is available for use only under signed agreement with Variable, Inc.


## Release Notes

### 2.0.0
* Updates Electron & Electron-Forge to current (Forge 6 & Electron 7) 
* ***IMPORTANT*** Requires user to delete package-lock.json, delete node_modules, and re-run npm install:
```shell
        #!bash
        $ cd ~/code/js/electron/dongle-demo-electron/
        $ rm -rf node_modules
        $ rm package-lock.json
        $ npm i
```
* ***IMPORTANT*** Note the different commands to start app and package app! Also dongle_server folder moved into ./static.

### 1.X.X
* Initial releases based on Electron-Forge 5.x & Electron 6