const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

console.log('\n\tcompiling for environment ', process.env.NODE_ENV, "\n")
module.exports = {
  /**
   * This is the main entry point for your application, it's the first file
   * that runs in the main process.
   */
  entry: './src/main.js',
  // Put your normal webpack config below here
  module: {
    rules: require('./webpack.rules'),
  },
  plugins: [
    new webpack.DefinePlugin({
      '__static': `"${path.join(__dirname, '/static').replace(/\\/g, '\\\\')}"`
    }),
    new webpack.DefinePlugin({
      '__production': process.env.NODE_ENV === "production"
    }),
    new CopyWebpackPlugin([
      {
        context: './static/',
        from: '**/*',
        to: path.resolve(__dirname, '.webpack', 'main', 'static'),
        force: true
      }
    ]),
  ]
};
