
# **Variable Bridge**

## Introduction
With the Variable Bridge, now Windows, MacOS, and Linux based computers are able to connect to Variable's Spectro 1 and Color Muse Pro devices. Desktop applications can interface with the Variable Bridge via the dongle-server executable (via a tcp socket, using JSONL for it's communication protocol). This document details the API communications for controlling the dongle-server. 

Additionally, a demo application is available for both Windows & MacOS, written in Electron-js, that demonstrates how to start & interface with the dongle-server binary to perform common tasks with the Spectro 1. 

The dongle-server binaries & demo app are listed in Downloads. 

It is a common requirement for an end user to install device drivers for the server, and as such can be downloaded from https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers

## Installation Requirements
 - Mac OSX, Windows Vista or later
 - 1 GB of ram
 - 20 MB per licensed device
 - Read and write access to file storage


## Socket Communication 
Communication between the client and the server occurs through a tcp socket, and both the client and server share a terminating character, '\n',and JSON format. JSON attribute names use a c style naming convention. Commands may have parameters, and are described in their respective sections under 'Command Set'. For handling reading data from the socket, it is recommended to check for multiple commands sent from a single socket read, by checking for the terminanting character, '\n'. Command names are case-insensitive.

**COMMAND**
```json
{
        "command": "name_of_command",
        "parameters": { },
}
```

**RESPONSES**

Similar to commands, responses share a json format.  A client receives a response, and events. The only difference between them is that events are sent without the client sending a command. For example, when a Bridge is disconnected an event is sent. Events and responses both share the same json format.
```json
{
    "event": "GetDongle",
    "error_code": "vi-*-*",
    "payload": {  }
}
```
`event` - always present. The name of the command or event.

`error_code` - this is optional, and when present indicates an error with the described `event`.

`payload` - this is optional, and when present is the response or events data. This is described in each command section.

---


## Command Line Configuration 
All configuration flags are passed through command line arguments.

`help` - lists the available command line arguments and there descriptions.

`port` - the port the server is to listen on. (Default: 9100)

`address` - the address the server is to listen on. (Default: localhost)

`verbose` - enables logging to stdout. 

`keep_alive` - enables bluetooth keep alive. Supported bluetooth devices have an inactivity timer of 5 minutes, and then disconnects. When set to true, this holds the connection open until a manually disconnection, or a bluetooth error. (Default: false)


## Environment Variables
`VARIABLE_LDIR` - The environment variable can be used to set the license folder the server uses. If specifying a custom path, the folder must be created, and read and write access granted before running server. 
    
    - Windows: `%LOCALAPPDATA%\Variable\Dongle` 

    - Mac OSX: `$HOME/Library/Caches/Variable/Dongle`
    
    - Linux: `$XDG_CACHE_HOME` or `$HOME/.cache/Variable/Dongle`

---
# **Command Set**

## Get Dongle Status
Responds with state information for an active serial device. When a Bridge device is present and has a bluetooth connection the remote bluetooth devices general hardware information is populated in `connected_devices`. Each bluetooth connections is queried and refreshed with battery and charging information. 

This command can also be sent as an event. A server has a routine serial monitor cycle looking for changes in serial connection, and begins monitoring at startup. Thus, when a new Bridge is detected a connection is attempted. While a server has an active serial connection any additional Bridges are ignored. The serial monitor uses 1 second intervals  before starting the next monitor cycle, and while a serial device is present it is  3 seconds.
**COMMAND**
```json
{
  "command": "GetDongle"
}
```

**RESPONSE**

Example of connected serial device with no bluetooth connection.
```json
{
  "event":  "GetDongle",
  "payload": {
    "status": "connected",
    "dongle_id": "COM1",
    "firmware_version": "70.06",
    }
}
```


Example of connected serial device and bluetooth connection. This can be sent as an event if a serial connection occurs while a socket client is connected. 
```json 
{
  "event": "GetDongle",
  "payload": {
    "connected_devices": [
      {
        "battery": {
          "is_charging": false,
          "is_charging_complete": false,
          "level": 100,
          "voltage": 4.157
        },
        "device_type": "spectro",
        "firmware_version": "60.23",
        "handle": 1,
        "rssi": -56,
        "serial": "0702815ECFD0",
        "spectro": {
          "is_calibrated": false,
          "last_calibration_scan_count": 3086,
          "lifetime_scan_count": 6157
        },
        "updated_at": "2019-05-17T13:44:00.1378109-04:00"
      }
    ],
    "dongle_id": "/dev/cu.SLAB_USBtoUART",
    "firwmare_version": "70.00",
    "status": "connected"
  }
}
```


Example of a serial disconnection event. (e.g. usb cable shorts, user unplugs serial device)
```json
{
  "event":  "GetDongle",
  "error_code": "vi-serial-disconnected",
  "payload": {
    "status": "disconnected"
  }
}
```

## Installing Licenses
Installs a single license file. Before using a Spectro device, the license file located on the removable storage inside packaging must be provided to the server. If a request is made for Spectro device without having first copied the file into the license directory, error code `vi-missing-license` is sent. 

**COMMAND**

`file` - an absolute path to a license file on the local machine. This file is provided to the customer on the flash drive provided in the Bridge packaging.

`overwrite` - by setting this flag to true, enables overwriting an existing license. Defaults to false, and is recommended for advanced usages only.
```json 
{
  "command":"CopyLicense",
  "parameters":{
    "file":"/Users/xxx/temp/D102BCD0F00.json"
  }
}
```

**RESPONSE**
```json
{
  "event": "CopyLicense",
  "payload": {
    "serial": "D102BCD0F00",
     "file":"/Users/xxx/temp/D102BCD0F00.json"
  }
}
```

**RESPONSE ERRORS**

`vi-invalid-parameters` - occurs when `file` is not present or is a directory.

`vi-failed-license-transfer` - occurs when the transfer failed. `payload` contains the source `file` that failed the transfer. This is typically because a corrupt or bad file was provided.


## Deleting Licenses

Deletes a license file, where the client provides a serial number. The response does not error.

**COMMAND**

`serial` - specifies the device's license file to delete by serial.

`delete_all` - when present and true, ignores the `serial` attribute, and deletes all licenses. (Recommended to not use)
```json
{
  "command": "DeleteLicense",
  "parameters": {
    "serial": "1AEC6EA80D00",
    "delete_all": false
  }
}
```

**RESPONSE**
```json
{
  "event": "DeleteLicense",
  "payload": {
    "serial": "1AEC6EA80D00"
  }
}
```


## Get Server Configuration
Responds with a configuration set at startup. During development, it might be useful to start the binary with additional configuration flags. Flags are set by using the json attribute name. (e.g. `--port 3101 `). All flags are desribed using a help flag. `--help `

**COMMAND**
```json
{
  "command": "GetConfiguration"
}
```

**RESPONSE**

`keep_alive` - this flag is for maintaing longer than 5 minute connection times. (Default: false)

`verbose` - enables server verbosity to stdout. (Default: false)

`port` - specify a port for the server to listen for incoming connections on. (Default: 9100)

`address` - specifies a network address to listen for incoming connections. (Default: localhost)

`serials` - a list of licensed serials, and are available for general usage. This cannot be set via command line, but with license installations.

`license_dir` - license directory. Configures the directory to store and monitor for license files. The process will need read and write access to the directory specified. (Defaults: %LOCALAPPDATA%\Variable\Dongle)

`devices` - a map of each serial and corresponding device info, such as device type and specular component.

```json
{
  "event": "GetConfiguration",
  "payload": {
    "address": "localhost",
    "license_dir": "/Users/xyz/Library/Caches/Variable/Dongle",
    "port": 9100,
    "verbose": true,
    "version": "0.1.1",
    "keep_alive": false,
    "serials": ["12AB45BAEF34"],
    "devices": {
      "12AB45BAEF34": {
        "device_type": "spectro",
        "specular_component": "included|excluded"
      }
    }
  }
}
```

## Bluetooth Discovery
A client starts and stops a bluetooth discovery. Both requests will respond with a basic acknowledgement response.  A stop bluetooth discovery is always sent on start of connection, and if a client sends `StopBluetoothDiscovery`.  One notable mention, is if a license file is missing, but a bluetooth device is discovered, there is no error broadcasted. This can be seen if `verbose` logging is enabled.

**COMMAND**
```json
{
  "command": "StartBluetoothDiscovery|StopBluetoothDiscovery",
}
```

**RESPONSE ERRORS**

`serial port closed` - connection to the serial device is no longer present


**EVENTS**

When a peripheral is discovered, an event is broadcasted to all clients.

`rssi` - the signal strength in dB

`serial` - serial of the peripheral discovered. This serial is used in all subsequent commands.

`device_type` - the type of device

`is_licensed` - a boolean that determines if the license file has been installed for the discovered device.

```json 
{
  "event": "DiscoveredPeripheral",
  "payload": {
    "rssi": -75,
    "serial": "1AB05E9DF00",
    "device_type": "spectro|color muse pro",
    "is_licensed": true
  }
}
```

 ## Connecting to a bluetooth device
A connection can be made with or without a discovery command. However, a discovery stop is always sent before a connection has been started. Bluetooth connections can reliable be held, with limited interference, up to 18 feet.

Bluetooth connection is a two stage response. 
```
1. Client sends `CONNECT` to server.
2. Server intiates bluetooth connection
3. Server responds with `CONNECT` as an acknowledgement that the connection has started.
4. Bluetooth connection is established.
5. Server sends `DeviceConnected` back to the client.
```

While up to 4 concurrent bluetooth connections are supported, it is recommended to connect to a single connection at this time as there are known issues with multiple connections.

**COMMAND**
```json
{
  "command": "Connect",
  "parameters": {
    "serial": "AF12BCD9C00F"
  }
}
```


**RESPONSE**

An acknowledgement is sent back as an indication the connection process has started.
```json
{
  "event": "Connect",
  "payload": {
    "serial": "AF12BCD9C00F"
  }
}
```

**RESPONSE ERRORS**

`vi-max-connections` - The maximum amount of devices are already connected. A maximum  of 4 concurrent connection can be made. 

`vi-invalid-parameters` - an invalid serial number.

`serial port closed` - the serial connection has been lost.

`vi-missing-license` - the serial requested to be connected to is missing a license file.  The `serial` is in the payload.

`vi-connection-timed-out` - A connection attempt has at most 5 seconds to establish the connection.

```json
{
  "command": "DeviceDisconnected",
  "error_code": "vi-connection-timed-out|serial port closed|vi-missing-license|vi-invalid-parameters",
  "payload": {
    "serial": "D702805ECF10"
  }
}
```

**EVENTS**

`rssi` - signal strength in dB.

`level` - The battery level from 0% - 100%. The following levels are recommended for prompting a user.
   * (12.5% < x < 37.5%) alert user to charge their device.
   * (x < 12.5%) alert the user to charger their device, and disconnect from the remote device.

`voltage` - The voltage level of the battery measured in Volts.

`handle` - ignore client side.

`last_calibration_scan_count` - the scan count when the last calibration occured. 

`lifetime_scan_count` - the number of scans this device has taken.

`is_calibrated` - indicates if a device is already calibrated. This value is computed by `lifetime_scan_count` - `last_calibration_scan_count` <= 1000.

`device_type` - valid values are "spectro" and "color muse pro"

 ```json
{
  "event": "DeviceConnected",
  "payload": {
      "rssi": -65,
      "battery": {
        "is_charging": true,
        "is_charging_complete": false,
        "level": 100,
        "voltage": 4.249
      },
      "device_type": "spectro",
      "firmware_version": "60.23",
      "handle": 1,
      "serial": "D702805ECFD0",
      "spectro": {
        "specular_component": "excluded|included",
        "is_calibrated": true,
        "last_calibration_scan_count": 1695,
        "lifetime_scan_count": 1695
      }
    }
}
```

A connection attempt failed to find the remote peripheral. 
```json
{
  "event":"DeviceDisconnected",
  "error_code":"vi-connection-timed-out",
  "payload": {
    "serial": "-",
    "disconnect_code": "0000"
  }
}
```


## Disconnecting from  a bluetooth device
It is recommended to keep track of user driven disconnections. This allows applications to precisely identify remote disconnections or connection errors such as device out of range and other forced remote disconnections.

**COMMAND**
```json
{ 
  "command": "Disconnect", 
  "parameters": { 
    "serial": "90CF6A9FFD90"
  }
}
```

**RESPONSE ERRORS**

`serial port closed` -  The communication to the serial port has stopped.

`vi-invalid-parameters` - an invalid serial number.

`vi-missing-license` - the serial requested is missing a license file. 

`vi-device-disconnected` - a request to a disconnected device.
```json
{
  "event":"DeviceDisconnected",
  "error_code":"vi-connection-lost",
  "payload": {
    "serial": "-",
    "disconnect_code": "0000"
  }
}
```

## Color Scanning
Initiates a color scan to a connected bluetooth peripheral. A scan response's lab space is configurable, by specifying a `lab` space. Valid lab spaces are listed below. 

For optimal scan accuracy, limit scan requests to 1 request / 5 seconds. 

**COMMAND**
```json
{ 
   "command": "Scan", 
   "parameters": { 
     "serial": "D702805ECFD0",
     "lab": {
       "illuminant": "D50|D65|A|B|C|F2|F7|F11",
       "observer": "TEN_DEGREE|TWO_DEGREE|2°|10°
     }
    }
}
```

**RESPONSE ERRORS**

`vi-missing-license` - occurs if a client requests to scan a serial number of an unlicense device.

`vi-bluetooth-device-not-connected` - the requested device is no longer connected.

`serial port closed` - the active serial port has been closed.


**RESPONSE**

`curve` - This is the points on the spectral curve. A specific wavelength can be calculated by `step`*i + `start`. A specific index for a given wavelength can be calculated by (`wavelength` - `start`) / `step`. This attribute is only present when `device_type` is spectro.

`start` - is a constant of 400 nm, and indicates the starting wavelength of a `curve`  array.  This attribute is only present when `device_type` is spectro.

`step` - is the wavelength step between array items in `curve`.  This attribute is only present when `device_type` is spectro.

`lab` -  represents CIE Lab space as annotated by `illuminant`, and `observer`. This attribute is always present.

`hex` - representation of the color as a rgb hex code using sRGB profile from `illuminant` and `observer` space.

`sense_values` - are used when sending a `SetCalibrationScan` command.

`device_type` - indicates the type of device. ("spectro" or "color muse pro")

```json 
{
  "event": "Scan",
  "payload": {
    "device_type": "spectro",
    "batch": "s1-1",
    "start": 400,
    "step": 10,
    "curve": [
      0.397981339085098,
      0.5456726467773839,
      0.5870447256170673,
      0.5971978304953154,
      0.6012153168850922,
      0.6113773206357005,
      0.6245757151624513,
      0.6377515236998466,
      0.6469966982440257,
      0.6519485605107644,
      0.6494168431749697,
      0.6341654677788521,
      0.6132449664010986,
      0.5888088451740104,
      0.5622473985790296,
      0.5327512472023951,
      0.5044738789739656,
      0.4796054929932459,
      0.462791755132651,
      0.45055885974552884,
      0.4339102732245163,
      0.41625702637493467,
      0.4062701716855791,
      0.4029100531337358,
      0.4051766206376034,
      0.40771144291066513,
      0.4135652024787718,
      0.42055920951859627,
      0.4287253266601045,
      0.4345314762532571,
      0.4436751974373851
    ],
    "hex": "#8DC9ED",
    "lab": {
      "L": 77.93663,
      "a": -11.20893,
      "b": -8.009456,
      "illuminant": "d65",
      "observer": "10°"
    },
    "model": "11.0",
    "scan_count": 1696,
    "sense_values": [
      0.4460822461280232,
      0.4985580224307622,
      0.542595559624628,
      0.5361257343404288,
      0.4611276417181659,
      0.40076295109483484,
      0.3751735713740749,
      0.3948271915770199,
      0.47177843900205996,
      0.5399862668802929,
      0.5645380331120775,
      0.542397192339971,
      0.45850308995193406,
      0.3936522468909743,
      0.3698023956664378,
      0.39252307927061875,
      0.49184405279621574,
      0.5586175326161593,
      0.5779201953154802,
      0.5481040665293355,
      0.4652475776302739,
      0.3935912108033875,
      0.37370870527199207,
      0.3915617608911269,
      0.509071488517586,
      0.5667963683527886,
      0.5846646829938201,
      0.5481956206607157,
      0.46135652704661634,
      0.3934691386282139,
      0.37338826581216145,
      0.39201953154802777
    ],
    "serial": "D702805ECFD0"
  }
}
```


## Calibration
Spectro One, device type "spectro", requires a sample scan on the white tile, and two verification scans provided in it's packaging. Calibration is identified by inspecting the `spectro.is_calibrated` connected device item array from a `GetDongle` response.

A recommended flow for Spectro One calibration is outlined below:

```
1. Prompt user to scan white tile from Spectro packaging. 
2. Request `SCAN` 
3. Store sense_values for use later.
4. Prompt user to scan green tile from Spectro packaging.
5. Store `sense_values` for use later.
6. Prompt user to scan blue tile from Spectro packaging.
7. Store `sense_values` for use in next step
8. Create `SETCALIBRATION` command from  values in steps 3, 5, 7.
9. Send command and wait for response.
```

Color Muse Pro, device type "color muse pro", requires a single sample scan on the white tile provided in it's packaging. 

A recommended flow for Color Muse Pro calibration is outlined below:

```
1. Prompt user to scan white tile from Spectro packaging. 
2. Request `SCAN` 
3. Using the sense values, set the `white_sense_values` attribute, and create `SETCALIBRATION`. All other attributes in `sample_scans` are ignored.
9. Send command and wait for response.
```

Device's type can be verified by requesting `GetDongle`, and inspecting the `connected_devices.device_type`


**COMMAND**

`serial` - the serial of the peripheral to perform calibration for.

`scans` - attribute values on this originate from a `SCAN` response's `sense_values` attribute. Only set the arrays that are in use for this device_type.
```json
{
  "command": "SETCALIBRATION",
  "parameters": {
    "serial": "D602815ECFA0",
    "sample_scans": {
      "white_sense_values": [],
      "green_sense_values": [],
      "blue_sense_values": [],
    }
  }
}
```

**RESPONSE**
```json
{
  "event": "SetCalibration",
  "payload": {
    "serial": "D602815ECFA0",
    "calibration_result": "success"
  }
}
```



**ERROR RESPONSE**
All error codes, indicate the calibration process did not complete sucessfully.

`vi-missing-license` - a request has been made for a serial number with no license file.

`vi-bluetooth-device-not-connected` -  a request specified a serial to a disconnected device, or  lost connection during the calibration process.

`vi-invalid-parameters` - There are a few situations where this error code may be returned. All of which are typically thrown during development.
  1. The sample scans attribute is missing one or more sense values. (e.g. *green_sense_values*, *white_sense_values*, *blue_sense_values*)
  2. The *white_sense_values* array provided is not the correct size. 

`vi-failed-white-tile-calibration` - The white tile scan was not within tolerance. Recommended course of action is to prompt user to rescan all calibration tiles from Spectro packaging.

`vi-failed-green-verification` - The green tile scan was not within tolerance. Recommended course of action is to prompt user to rescan all calibration tiles from Spectro packaging.

`vi-failed-blue-verification` - The blue tile scan was not within tolerance. Recommended course of action is to prompt user to rescan all calibration tiles from Spectro packaging.

`serial port closed` - The serial port was closed during the calibration process.

## Button Events
When a button is pressed on a connected Spectro a ButtonDidPress event is broadcasted with the remote peripheral's serial.
```json
{
  "event": "ButtonDidPress",
  "payload": {
    "serial": "A07BCD0D2F02"
  }
}
```

## Shutdown Server
This disconnects all bluetooth connected devices, before terminating the process. Thus, does not have any response. If the process is manually killed by a parent, bluetooth connected devices will stay connected until the Bridge has been removed from the machine. 

**COMMAND**
```json
{
  "command": "Shutdown"
}
```

# Server Binary Downloads 
  **v0.4.2**

  [Windows](windows/variable_dongle_server.exe ':ignore')
    
  [Mac OSX](osx/variable_dongle_server ':ignore')
    
  [Linux](linux/variable_dongle_server ':ignore')

# Demo Applications
  [Simple C# Client](simple_c_sharp.zip ':ignore') (solution requires a server binary to be running before execution.)
  
  [Demo Application Source (electron-js)](https://bitbucket.org/variablecolor/dongle-demo-electron ':ignore')

  [Demo Application Downloads](https://bitbucket.org/variablecolor/dongle-demo-electron/downloads/ ':ignore')


# Additional Notes
 - It is recommended to write one command json at a time.
 - Bluetooth connections persist through server restarts, and will disconnect if power is lost from the pc.
 - Binary needs user level read and write access to these locations:
    - **Windows** `%LOCALAPPDATA%` or `C:\Users\%USER%\AppData\Local`
    - **Mac** `$HOME/Library/Caches`
    - **Linux** `$XDG_CACHE_HOME` or `$HOME/.cache`

- Version 0.3.5 -> 0.3.6 upgraded low level serial communication library for all platforms.

- How do I verify driver installation on Max OSX?

```
checks if user consent is enabled
spctl kext-consent status 

verify silabs driver is loaded with a version
kextstat | grep silabs

shows current user-approved extensions (useful for looking for conflicts)
kextfind /Library/Extensions -loaded
```

# Changelog

  Version 0.4.2
   
    * CRASH: If the serial port failed to open, server may crash due to a segfault.

  Version 0.4.1
  
    * Alters bluetooth device discovery. All devices are sent back, and marked with `is_licensed`, as true or false.

  Version 0.4.0
    
    * Adds RTC Control
 
    * Fixes server not responding to GetDongle command on startup.

  Version 0.3.6
    
    * Upgrades serial communication library